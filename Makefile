BASEURL=https://nslisica.neocities.org
GENs:=$(subst .md,.html,$(shell find . -name '*.md')) wall.html wall.atom sw/wall.html sw/wall.atom

all: $(GENs)

%.html: %.md _markdown _nav.html _footer.html _filter/djot-meta.lua
	./_markdown $< -o $@

%: %.pp
	pp $< >$@

wall.md: wall.txt
sw/wall.md: sw/wall.txt

wall.html: wall.md
	./_markdown --include-in-header 'data:,<link rel="alternate" type="application/atom+xml" href="wall.atom"' -o $@ $<

sw/wall.html: sw/wall.md
	./_markdown --include-in-header 'data:,<link rel="alternate" type="application/atom+xml" href="wall.atom"' -o $@ $<

wall.atom: wall.txt
	./_bin/twt2atom -b $(BASEURL) -a 'Александар Средојевић' -e 'ferrumite666@gmail.com' -t 'Шта се овде дешава?' wall.txt wall.atom

sw/wall.atom: sw/wall.txt
	./_bin/twt2atom -b $(BASEURL) -a readmemyrights -e 'ferrumite666@gmail.com' -t 'readmemyright'\''s wall' sw/wall.txt sw/wall.atom

deploy: all
	./_deploy

wall:
	./_bin/newwall wall.txt

sw/wall:
	./_bin/newwall sw/wall.txt

clean:
	rm -f $(GENs) wall.md sw/wall.md

.PHONY: all deploy wall sw/wall clean
