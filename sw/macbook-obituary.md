# air2017s-macbook-air

2017--2025

The world was very different to me back in 2017.
I knew about computers,
I programmed in python,
[bgt](https://audiogames.net/db.php?id=Blastbay+game+toolkit)
even had basic grasp on html but nothing further than that.
I wanted to install linux because it was cool,
only touched the windows command line when an online guide told me to,
and I wouldn't understand or believe a single thing
I've ended up saying these days.

I wanted to buy an apple computer,
and finally settled on the cheapest MacBook air at the time,
with 8gb of ram and 128gb of storage.[^perspective]
Other contenders included a Mac mini
and various other MacBooks.
One of a few opinions I and my younger self agreed on
is that windows is a terrible operating system,
and that's the big reason for my apple fan phase.

[^perspective]: I bet that by 2030 readers of this article will be asking "how"??
  And just to blow their minds even more,
  these specs were low for 2017.

It took me time to switch to the macbook.
My del was getting unbearably slow
but it had one thing the macbook didn't: games.
Now, those weren't mainstream games,
those were [audiogames](https://audiogames.net).
and they were almost all exclusive to windows at the time
(and using wine wasn't an option at the time
if you weren't ready for pain).
In contrast, the mac had relatively little software I cared about,
and it wasn't familiar to me.
I had to use the MacBook for school
because automatic windows updates would always happen when I needed to use the del the most.
And I slowly stopped playing audiogames and wasting time on twitter.
Finally the MacBook became my main computer.

At first I tried to use my MacBook for making music,
garageband is my main argument for getting a mac in the first place after all.
It went ok for a little while,
at least I can say I made music,
but garageband's paradigm just didn't really suit me.
There was a lot of targetting, dragging / dropping,
precisely pressing record,
and none of it suited the programmer in me.
Not to mention that I couldn't have gone much further
without buying expensive equipment.

Then at the beginning of quarantine I began learning html again.
This time I went further and learned javascript and php.
The exact order of events after that
I can't remember,
but at some point I got comfortable with the terminal and vim,
learned more and more programming languages,
and found out about the evils of proprietary software.
That interest in programming hasn't stopped since,
and it's been helped by the fact
that under all the flashy icons and animations
MacOS was a unix like (mostly) any other.
I could never install xcode on my computer
(it was over 10gb just to download)
so I had to stick to minimal software and development tools.
this naturally lead me to learn about some amazing software
and people making it.
I learned so much about programming
just by learning how my favorite programs worked.

That's only one half of the story.
The other half of my interests,
mainly card games and other physical board games,
came much after.
I played card games before,
but the lack of games for mac made me look for entertainment elseware,
and I would have likely never looked into them again
if my mother hadn't found my two packs of braille playing cards.
I began to play patience games with them,
of course starting with klondike
and then discovering others.
Again, looking all over the internet
helped me find various resources,
including youtube channels
and, of course, the treasure trove that is
[pagat.com](https://pagat.com/).

It surprises me how buying a MacBook
made me take such a course in life.
Back in 2017 I didn't know about any of this,
I just wanted the cheapest computer with the apple logo.

I did everything in my power to keep the laptop usable,
a big part of it was because I felt bad for spending so much money on it,
and I was always
(and still am)
very much a penny pincher.
my indecision over what laptop to go with next made it even harder.
I replaced the battery, the charger,
used an external keyboard because the internal one failed,
and it would have only gotten worse I'm sure.
I finally gave up when the charger fell apart
and refused to come back together stably.

I charged it for the last time
on 2024-02-20,
and it's battery finally ran out the next day.
I might let my mother inherit it,
or we might throw it away completely.
I backed up everything on it that was important already
but it will take me time to fully switch to my new laptop.

It'll be followed by my asus laptop malpertuis
(which holds my arch linux vm iusearchbtw)
and my raspberry pi named [boogie](boogie.html).

In memorium.
