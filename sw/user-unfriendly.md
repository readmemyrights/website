# In praise of windows' user friendliness

Whichever operating system one uses or prefers,
everyone who works with technology takes it as an obvious fact
that windows is a user-friendly operating system.
Even harshest critics of it don't argue against it,
and instead argue that their preferred operating system
is just as friendly to users.

And really, how can one argue?
It's got a desktop, start menu, and a clock in the corner,
everything is done by clicking and selecting from menus.
Vast majority of windows users never have to type in a command,
edit configuration files,
or indeed know such things exist
or imagine that someone might prefer
to interact with the os that way.

Of course, the strength of a program
(including an operating system)
_isn't_ in how much it can do
with the few services it provides,
but how, with a myriad of specialized interfaces,
it manages to do nothing more.
Why make it easy to write scripts
for generation and sending of automated emails
when a mail merge utility
built into one program
that relies on dozens of others in its suite
will look and sound much better in demos?
As we all know,
"An ideal program is like a tunnel bored through rock",
so an operating system
should be a system of caves
that lead to things an "average" user would like to do,
and nothing more.

Nothing can better demonstrate this
then my experience today
with setting up keyboard layouts.
Truly something an average (bilingual) user needs to do.
I write Serbian (both cyrillic and latin scripts) and English text
on a daily basis,
So of course I need to go to settings -> language
and install language packs for those languages,
and not, for example, Control Panel -> keyboard,
which is for other keyboard settings.
never mind that keyboard layouts aren't necessarily tied to language,
or that for over a decade windows still has 2 applications to change system settings
(not counting the dreaded yet ever-useful registry editor),
This just works and that's how computers are meant to work.

But oh wait,
one day I find there are much more keyboard layouts in my list.
There is Serbian for both Montenegro and Bosnia with latin and cyrillic versions each,
which makes sense, I must have just forgotten to uncheck some checkboxes.
But then there's English Zimbabwe and a Russian keyboard layout as well?
Oh whatever, computers are magic,
a wolpertinger probably sprinkled some fairy dust and made that happen.
I only need to go to the same place
as when I first added keyboard layouts,
and just click on delete, or remove, or whatever else
for the unwanted layouts.

Well, that's strange, only the languages I added are there!
I look in the options for each language and none of the extra layouts are there either,
and the unwanted layouts refuse to go
even when I remove the languages I added.
At this point, if I were on a wizard's os like linux
I'd take a deep breath and look through manual pages
to find about which configuration file keyboard layout settings are in,
and check what's written there.
But you can't do that in windows.
I meant, you don't need to,
since you can just type in your question into google
and pray that the microsoft support page that comes up
will actually answer your question.
It's like they heard "online documentation"
being praised back in the 70s
and never actually looked at what it meant at the time.

The microsoft support page just mentions what I already tried.
Not to worry though, because obscure forums are here to help,
and these ones don't hide answers behind an account registration.
They repeat what microsoft say,
and then describe the ultimate way to remove a keyboard layout,
with the registry editor.

The windows registry is a convoluted mess
that puts every other configuration system to shame.
It's a key value store
that uses something similar to file paths for keys
(but they don't corespond to any files on disk)
and most values are obscure hex values apparently.[^plsnobully]
The keyboard layout options are spread between two keys,
and instead of human-readable layout names
they use hex ids one has to look up on another page.
How fortunate that the registry editor
asks me 'are you sure" when deleting keys.

[^plsnobully]: Of course, I know as much about the windows registry as the average user windows is supposedly friendly to.

Of course, after making such radical changes to the system,
you need to sign out and back infor them to take effect.
Wait, why does editing the registry require signing out,
but editing with settings doesn't?
Oh, whatever, computers are magic,
there's no rhyme or reason to their workings.
They are made by people after all,
and to err is human.
It's just impossible to make programs and systems
that don't do random unexplainable things once in a while,
or more often as the case might be.
Simplicity isn't something to strive for,
not even an unreachable ideal,
but a perfectionist nightmare.
As someone at microsoft must have said:
"Make things more complicated left and right, no harm can come of it".[^pisarev]

[^pisarev]: Probably paraphrasing [Dimitry Pisarev](https://en.wikipedia.org/wiki/Dmitry_Pisarev).