# I don't use software made by bullies

I often say something about "open source software"
and if you ask most people what it means
You'd get the impression
it's about what bytes are in the `LICENSE` file.
For me, that's the least important part of it.
The source itself is usually just a curiosity.
What's important is how the software treats its users.

And many "free" and "open source" programs today
treat their users like garbage,
as if it wasn't made for them but against them.
And then the developers behind those projects proudly show off
the disdain they have for them.
Here are only some of many examples:

* Mozilla developer network,
  One of the most comprehensive resources on web development online,
  introduced an "AI explain" feature
  that confidently gave false and misleading information.
  [the issue was reported](https://github.com/mdn/yari/issues/9208)([archive](https://archive.is/WYyl9))
  yet the maintainers who implemented the feature
  spent more time defending the clear failure
  demanding examples when they were already provided
  and then trying to shut up their community with numbers.
* For almost 18 years
  the GUI toolkit GTK,
  which is used by the desktop environment gnome,
  didn't have thumbnails in the file picker.
  [A bug was filed in 2004](https://bugzilla.gnome.org/show_bug.cgi?id=141154&)([archive](https://archive.is/3F7Lg))
  over the years it got migrated
  between different bug trackers
  [its final home being on gitlab](https://gitlab.gnome.org/GNOME/gtk/-/issues/233)([archive](https://archive.is/95c1S)).
  It finally got fixed and merged in
  but only after almost two decades
  of GTK developers passing the buck,
  calling everyone entitled for wanting this basic feature
  and even [breaking compatibility to prevent people from using a better file chooser](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/4829)([archive](https://archive.is/tpKXn)).[^gnome]
* Mozilla firefox, supposedly the "independent" and "freedom respecting"
    side of the browser wars,
  has over the years reduced user choice,
  first only letting options be accessed through `about:config`
  and then removing them all together.
  [User who voice their opinions get the usual excuses and `WONTFIX`es](https://bugzilla.mozilla.org/show_bug.cgi?id=873709)([archive](https://web.archive.org/web/20181012153654/https://bugzilla.mozilla.org/show_bug.cgi?id=873709)).[^mozilla]
* The developer of systemd, the most common [pid1][] for linux,
  is very careless about security vulnerabilities
  especially for someone that develops
  *the most important* part of a unix system.
  [This is just one example](https://github.com/systemd/systemd/issues/6237)([archive](https://archive.is/qljzi))
  where he blames _other_ tools
  for making invalid usernames that makes *his* software malfunction![^systemd]

[pid1]: https://en.wikipedia.org/wiki/Init
[^gnome]: This isn't an isolated incident within gnome either,
  just the most infamous one.
  A whole page could be written just about gnome developers and their antics.
[^mozilla]: Like with gnome, this is just the tip of the iceberg.
  Fortunately, [digdeeper already wrote an article all about them](https://digdeeper.club/articles/mozilla.xhtml)([archive](https://archive.is/SC68z))
[^systemd]: And again, this is just one example.
  I might just write an article about the misdesign of systemd,
  but until then
  [here's a whole website just about it](https://nosystemd.org)([archive](https://archive.is/67aqF)).

Let's be clear, all of those projects are "open source".
you can download, read, edit, and redistribute their source code
or the changes you make to it.
You can make a fork any time and release it as your own.
Many people did that already.
But the things I mentioned above is counter to,
if not the letter,
then the spirit of free and open source software.
The things you can read on those bug trackers
are not too different to the stock responses
you'd get on support systems of various proprietary companies,
And in the worst cases can devolve to the point
where it looks like the developers don't want to make good software
but instead be the robber barons of their own little thiefdom.

In the 80s and 90s,
people started making free software
to counter corporate robber barons of the time,
such as IBM, Microsoft, DEC, etc.
While the barons made bland special-purpose software, cryptic manuals
and sold them for however much money they could get away with,
projects like [gnu][]
and people like [Larry Wall][] and [Linus Torvalds][]
instead made their programs opened,
documenting their behaviors well
and making them easy to change and improve.
That's why perl ended up dominating the 90s web,
why linux powers the internet to this day
and why today's robber barons even have a community to rule over.

[gnu]: https://gnu.org/
[Larry Wall]: https://en.wikipedia.org/wiki/Larry_Wall
[Linus Torvalds]: https://en.wikipedia.org/wiki/Linus_Torvalds

Today we still have corporations
desperately trying to regain some of their own power
using these developers to weaken the community
and reshape the software they make in their own horrifying image.
Besides the attitude to its existing users,
they show their true colors in the way they attract new ones.
Instead of organically spreading by word of mouth
they flood online discussions with fake praises of their products.
Instead of beating their competitors on their merits
they lie, spread rumors, and slander their developers.
Instead of justifying their alternatives and reimplementations
they pretend they were the first and only ones
and that anything else is either inadequate or deprecated.
And when all that fails
[they call you every bad name the neoliberal establishment ever came up with](https://drewdevault.com/2021/02/02/Anti-Wayland-horseshit.html)([archive](https://archive.is/YuqK4)).[^drewbtfo]

[^drewbtfo]: while looking for that article
  I found out that searching for "I'm tired of this anti-wayland horseshit"
  Drew's article just barely appears on the first page of results
  under reddit, hacker news, and even /g/ making fun of him.
  I originally misspelled "horceshit" like I often do while searching,
  and then it didn't even appear.

We have a name for people who do those sorts of things: "bullies".
And just like bullies at school
or bullies in politics,
or bullies in the office,
the only way to stop them is to say no and stay away.
This isn't just because of what they say or how they act,
although that's good enough reason.
It's because I rely on my software working like I want it to,
and any breaking changes these bullies want to push
for their selfish petty reasons
is annoying at best
and debilitating at worst.

If that last sentence sounds overly dramatic,
I'm a blind person.
Many things I need to do,
I have to do it with computers.
There are many different components involved
so I can use computers.
If one of those components break,
one program could break,
or many of them could,
or I wouldn't be able to get passed the log in screen,
that is, not even be aware there was a log in screen.
Bullies like to dismiss customisations and theming
like it is for "ricers" and "tinkerers"
but for some people,
it really helps them.[^sowhat]

[^sowhat]: I however won't let them get away with the original argument
  It's not their choice whether "ricing" or "tinkering"
  actually makes someone more productive,
  that's up to the user.
  They, and their opinions on what interfaces should look like
  don't matter in the slightest.

That's also the reason why I can't
"separate the art from the artist" when it comes to software.
Programs evolve with time
and I actively rely on programs I use
instead of passively contemplating it like a song or a movie.
If one of these bully "developers"
decides that nothing should ever exist in
`~/writing/website`,
and update their software to delete everything in that directory
to "make `$HOME`s everywhere a cleaner place"
then I'd suddenly have to change my directory structure around
to appease their program
instead of the other way around.
Or maybe said bully "developer"
[decides that all Russians are responsible for what their government does and need to have all their files removed](https://www.bleepingcomputer.com/news/security/big-sabotage-famous-npm-package-deletes-files-to-protest-ukraine-war/)([archive](https://archive.is/Tdem2)).

In conclusion, when deciding which software to use
I'm most of all concerned at how it treats and how it will treat me,
and I judge that by the way the developer treats the users of his software.
when I make software I try my best to make it work,
then to make it keep working as it did,
improving without disrupting existing users.
Whenever there's a choice to make,
I leave it up to the user to choose.

I'd also like to use this website
to help programmers write more software like that..
I hope to see you around,
and take care.
