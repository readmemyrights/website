# Readmemyrights on software

> Writing software is neither a science or an art.
> It's a craft whose goal is to do the most with the least,
> Whose result is as strict and precise as science
> And as beautiful as art
>
> -- Me, a pretentious quote I just made up

I'm a computer user and programmer,
I'm especially interested in UNIX-like operating systems and tools.

I'll write more here, have pages for the projects themselves, some articles...
If you see this please remind me to continue working on this.

To follow updates to this website,
[check out the wall](wall.html).

## Software I made

I've written and maintain various programs:

* [unixutils](https://codeberg.org/readmemyrights/unixutils): a collection of
  unix utilities. A part of them is `env_helper`, a script I use every day,
  even if indirectly.
* [tdsr](https://codeberg.org/readmemyrights/tdsr):
  my fork of [the original tdsr](https://github.com/tspivey/tdsr).
  A terminal screen reader.
* [te](te.html):
  every real programmer has writen a text editor.
  I don't know if this counts though...

There are various others on my
[codeberg](htttps://codeberg.org/readmemyrights)
And you can see more of my open source contributions on
[github](https://github.com/readmemyrights).

## Articles about software in general

* [I don't use software made by bullies](bullies.html)
* [Interactive programming](interactive.html)
* [Questioning libraries](libraries.html)
* [teaching OOP](teaching-oop.html) -- dumb title, please email suggestions.
* [Left behind](left-behind.html)
- [Database migrations](database-migrations.html)
* [In praise of windows' user friendliness](user-unfriendly.html)
  -- applicable to many other things too.

## Software and hardware I discussed

* [Mastodon](mastodon.html)
* [My raspberry pi](boogie.html)
* [my MacBook (RIP)](macbook-obituary.html)
