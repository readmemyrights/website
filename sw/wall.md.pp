# Readmemyright's wall

This is my english and software focused updates page.
Here you can find updates about my website,
and thoughts and opinions that don't deserve a whole webpage.

The feed for the whole website
can be found [here](../wall.html).

If you'd like to follow updates,
you can use either [rss](wall.atom)
or [twtxt](wall.txt) from which this page and the atom feed are generated.

> Why don't you use twitter or mastodon like a normal person?

First off, I'm not normal.
Second, people on twitter aren't normal,
and [ditto for mastodon](mastodon.html).

#!
./_bin/twt2md sw/wall.txt
