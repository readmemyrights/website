# Left behind

I don't like to say I have an old computer.
The first laptop I ever used was brand new in 2005,
it's battery was dead and a key was missing in 2012
but it was clearly usable until at least 2016.
My second laptop,
the first one that I could call "mine"
came to me with windows 8 in 2013,
and while it's struggling under the weight of windows 10
as far as I know it still works somehow in my living room.
And besides computers, I have a
[plex talk](http://www.plextalk.com/)
I received back in 2012,
and I listen to audio-described crime shows
on it today.
Sure, it's internal clock somehow doesn't work at all,
it's battery is unpredictable to say the least,
and it has fallen one too many times
and the battery cover is threatening to fall off.
It has mulched some files on my sd card once,
and it doesn't recognise it at all sometimes.
But it does its job.
it does it better than any phone app I've heard of,
and transfering and finding things on it is not only easy,
but not too different from how the computer works.
All this to say that
my macbook is showing its age after only 7 years of use
and I'm having a hard time accepting it.
I didn't care when the track pad stopped working,
when whole keys on the keyboard stopped listening I connected an external one,
and I embraced minimalist terminal programs to not waste space.
Ungoogled chromium still works quite well,
even with some of the more bloated websites today.
I wasted money to fix the battery,
which only worked for about half a year.
I was going to fix the keyboard but it never happened.

But nothing can hold the rest of the world back,
first I couldn't upgrade to MacOS 13,
then I couldn't install new programs because the only worked on MacOS 13 or
later,
and eventually [homebrew stopped supporting MacOS 12](https://github.com/orgs/Homebrew/discussions/5603).
which meant no precompiled binaries,
which meant I had to compile everything I needed,
which meant I had to install llvm,[^llvm]
and installing llvm is all but impossible with how little space I have left.
In short, I'm now hopelessly stuck on older versions of my programs,
and installing anything new is likely to be impossible.

[^llvm]: Even though I already have llvm
    as a part of developer command line tools.

And when was MacOS 12 released?
Turns out
[its third anniversary is coming up](https://en.wikipedia.org/wiki/MacOS_Monterey).
Yes, it's been only 3 years.
In comparison,
[Windows 10 was released in 2015](https://en.wikipedia.org/wiki/Windows_10)
[and it will stop being supported by microsoft in 2025](https://archive.is/6AkYs).
That's 10 whole years,
and it's likely others will continue to support it for longer.

Of course, apple is infamous
for this sort of thing,
and if I was more aware of the state of computing in 2017
I would have likely settled for a windows laptop
and perhaps not be writing this post now,
but probably will still be a few years from now.
To be clear, it's not hard for me to buy a knew computer,
or to switch to it,
or anything of the sort.
I don't believe MacOS 12 has something newer computers don't,
and I don't think I'm soul bound with my computer.
I just don't want to throw things away,
especially when they're still quite usable
beyond missing a few mostly superficial features.
I care this way about all things I own,
if only to not spend too much
and because I'm aware that this laptop
won't get thrown into a black hole
when I'm done with it,
let alone made into something better.
It will just be dumped in some landfill
where it would pollute the earth for millennia.

Of course, I don't get much choice in the matter at this point,
I can hold out for perhaps a year longer before something critical breaks.
Maybe I can wipe it and let my family use it.
And then I'll have to finally switch to my new laptop.
If anyone opens a cybermonistaries
where I get to use good ancient software in peace and bliss
shill it to me.

ON 2024-11-20,
when I decided to update a few packages
that aren't yet a complete lost cause
homebrew greeted me with this lovely message:

> ```
> Warning: You are using macOS 12.
> We (and Apple) do not provide support for this old version.
> It is expected behaviour that some formulae will fail to build in this old version.
> It is expected behaviour that Homebrew will be buggy and slow.
> Do not create any issues about this on Homebrew's GitHub repositories.
> Do not create any issues even if you think this message is unrelated.
> Any opened issues will be immediately closed without response.
> Do not ask for help from Homebrew or its maintainers on social media.
> You may ask for help in Homebrew's discussions but are unlikely to receive a response.
> Try to figure out the problem yourself and submit a fix as a pull request.
> We will review it but may or may not accept it.
> ```

This message didn't appear before,
so this was a new addition,
they likely added it in
because there were people still using the old version of MacOS.

I find it funny how confrontational the message is,
It feels like they'd do anything
to not have to deal with a 3-year-old OS version.
It could be a fun copypasta maybe.
I'm not feeling like making any edits,
because I just remembered trying to compile gcc once
and how well it went.

On 2024-02-21, I could no longer delay the inevitable.
The macbook's second charger broke apart
and I can't deal with those things any more.
Rest in peace.

[The obituary](macbook-obituary.html)
