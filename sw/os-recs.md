# OS recommendations to blind people

I recently saw a video by "The blind life"
([youtube link](https://www.youtube.com/watch?v=raaGplE2qcQ))
and I found it so devoid of content
or anything resembling information
it inspired me to write a page
that does it properly.

I'll here focus on what I have experiences with,
so mostly screen readers and similar technology.
I did see options for low vision accessibility
in pretty much everything I used
but I can't say anything about how good they are.

## The jist of it

* Windows, Mac, IOS and android
  all come with a screen reader by default,
  although I strongly recommend installing [NVDA][] on windows.
* Applications that come by default on these systems
  are usually accessible,
  although some are better than others
  and I can't vouch for each one individually.
  At the very least the web browser, text editor and file explorer work,
  and other applications can be replaced.
* Third-party applications are a hit and miss on almost every platform,
  although web applications have better odds in accessibility's favor.
  Windows has the most software for sure,
  MacOS applications are usually ok,
  android, unless it's a very famous application
  it's not likely to work.
  I can't speak much for IOS.

If nothing I wrote above means anything to you,
go with windows computer and android phone.
Nobody got fired for going with windows,[^buyingibm]
and android phones are usually cheaper
and do the basic things right.

[NVDA]: https://www.nvaccess.org/
[^buyingibm]: Not everyone is familiar with the "nobody got fired for buying IBM" saying,
  and duckduckgo at least gives poor results when one searches.
  In translation: it can't go wrong,
  only fail a little occasionally.

## The contestants

I've used MacOS, Windows and some linuxes.
I used android, and IOS a long time ago.
I'll say a little about each
as far as non-accessibility features are concerned,
to know what to expect:

: Windows

  you know this one, whether you like it or not.
  It's the A-list celebrity that,
  despite sliding step by step into irrelevance,
  just refuses to die.
  The meteor that will kill this dinosaur is still in orbit.
  It's got a lot of applications still written for it,
  including software useful to blind people.
  It's also what most of the world still uses,
  outside of a few sectors.

: MacOS / IOS

  The operating systems on various apple products.
  Some people love them, others don't.
  Apple's garden has perhaps the tallest walls in the tech industry
  (don't use if you have an android unless you aren't ready for some headaches)
  and their products, even relatively older ones,
  are quite expensive, prohibitively so for some.
  For what it's worth, I'm typing this one an external keyboard
  connected to a 7-year-old MacBook whose internal one doesn't work
  and I can't say I regret buying it.
  It's not got as much software as windows,
  but it's got a good enough ecosystem.
  I haven't used IOS in a while,
  but most should apply there too.

: Android

  The other operating system for non-desktop devices.
  While the operating system is the same
  each phone manufacturer adds their own things and takes out others,
  an extreme case is Samsung who made their own screen reader.
  It's got a wide range of applications like iPhone,
  some of which are exclusive.

: Linux

  It's gotten a fair bit of attention from the blind community recently,
 at least among the ones interested in things like linux.
 It's not the easiest switch,
 but I recommend at least trying it out,
 if only for the experience of using another operating system.
 THe situation however is much more fractured than on android,
 which while not bad in itself
 makes it hard to give a consistent recommendation or critique.

: Anything else

  There are many things I haven't tried out,
 most famous of which is probably chromeos.
 Most "unix-like" OSes
 such as the BSDs should work similarly enough to linux,
 once you've gotten them running.

There are two things that also deserve a mention,
even though they aren't operating systems,
but are instead more like kinds of applications
that can run on most of them.

: Web applications

  I'm using this as a blanket term for everything that runs in a web browser,
  some are called "websites" others "applications"
  depending on what argument people are having.
  Given that every operating system has an accessible web browser,
  most well-designed web applications should also work on them.
  The most complex can be hit or miss depending on web browser
  but you can usually be pretty sure that it will work
  with either firefox or chrome-based browsers,
  and every OS you're probably considering can run either.

: Terminal applications

  Anything that runs in a terminal, console, command prompt, etc.
 Again, nearly every operating system has one of those,
 although some might require more configuration to make them fully accessible.
 Some might think they're a relic of the past,
 or only for advanced and knowledgeable users,
 but it's still an unmatched environment in some aspects.
 I might write more about it some day.

I'll now talk about accessibility on each.

### Windows

Assuming you've figured out where the power button is and pressed it,
the first step is to turn on narrator
and install another screen reader.
Earlier versions of windows played a noise when they turned on,
but at some point it got removed so you mostly have to guess when it's ready,
or "blindly" try one of the steps below until it works,
or ask for someone to help you out.

How to turn on narrator depends on whether you're on the log in screen
or if you get logged in automatically.
Here are some ways I remember working.

* Windows+u: opens "ease of access",
  and on the log in screen it reads allowed the options.
  When you're logged in it doesn't speak,
  but pressing alt-n should open narrator.
  It's a little bit different on older versions of windows
  but the shortcut is the same.
* Windows+r, type in "narrator" and press enter.
  This should work on any version and has always worked for me.
  except on the log in screen,
  where there's no run dialog box.

After this stumbling block however
Windows is pretty usable.
Narrator is still quite janky
but since Windows 10 it can be used full time,
but I still recommend installing another screen reader.
Windows' biggest accessibility selling point
is its screen readers
and in particular how customizeable they are.
All of them have a basic help facility,
pressing `insert+1` (on the number row)
gets into a special mode where you can press any key
or combinations of them,
and get told what it would do.
Pretty much all screen reader commands use the insert key,
or capslock if they were so configured.

Windows accessibility is hit and miss.
While usually it works well,
it often happens that microsoft decides to make a new thing for applications
and break third-party screen readers in the process
(although it gets fixed fast enough).
The accessibility of their programs also leaves a lot to be desired,
and screen readers often have to work hard making them work.
Third party applications are somewhat better,
although there are always misses
especially on the microsoft store.

Chrome and firefox are available,
along with their various forks.
which includes microsoft edge.

It's probably the safest bet
if you aren't familiar with any other operating system.
And it has the most blind people using it
so there is already a lot of add-ons and programs
to make using it easier.

### MacOS

All apple systems have a screen reader called "Voice over"
that can be turned on at any time by pressing `cmd+f5`
where `cmd` is the key next to the space on the left.
You can get a quick start guide by pressing
`ctrl+option+cmd+f8`, so all three keys next to the space bar
and then f8.

The system itself is pretty much completely accessible,
although voice over doesn't interact well with the default terminal application.
I recommend using [tdsr][] if you want to use the terminal applications.
Third party applications depends a lot on how they're made.
Most applications I ended up installing were quite usable,
which isn't that much.

[tdsr]: https://codeberg.org/readmemyrights/tdsr "shameless self-plug"

For web browsers, there's safari,
and various google forks work quite well.
I had trouble trying out firefox however.
One thing to note when using chromium-based browsers
is that arrow keys don't let you move through the text like usually,
not until you press f7 to get into karet browsing.

It's biggest accessibility drawback is its screen reader,
and the software available for blind people in general.
Voice over has got a weird keymap
that even I, who have used it for years now, struggle to remember sometimes.
It's also not very extendable, at least not without learning applescript,
which, like with anything that starts with "apple",
has little to no documentation
and not many people know in the first place.
This makes adding new features to voice over,
something very common with windows screen readers,
an extreme rarity with voice over.

It was a while since I used IOS, so I can't say much about it.
Tripple pressing on the home button turns it on.
It has braille screen input at least,
which is much better than the normal touch keyboard, if it works.
I do remember less applications being accessible however.

### Android

Android is what I'm currently using,
and even though I only used two different phones until now
there are quite a few difference between manufacturers.
Pressing volume up and down keys together for about 3 seconds
turns on the screen reader,
which is talkback on most phones,
and voice assistant on Samsung.
Both work well enough,
but they are very different in what they support.

Recent versions of talkback have a braille keyboard
you can use to type text in any text field.
It works great for simple things,
some of the more advance things require somewhat obscure gestures
but nothing too bad.
Both talkback and the braille keyboard have tutorials
that you can access within talkback settings.

There's a third party screen reader called [commentary][]
which seems to be pretty popular among some android users.
It's main selling point
was better integration with third party braille keyboards
before talkback had its own.
It's also got much more features than regular talkback,
although most of those feel like bloat to me.
Bigger problem with it is the fact
it officially only seems to be distributed through its telegram channel
and there aren't many guides on how to use it.
What I want to say is,
using it isn't as seemless as, say, NVDA or JAWS are on windows.

Situation with third party applications might be the worst
on any mainstream platform,
if it's not a top 100 application
made by a big tech company
it's not likely to be accessible at all.
What's worse, some programs seem to work well on one phone
but not at all on others,
and I've noticed that some of them,
particularly mobile games,
like to make their own screen reader
that bypasses talkback completely,
include the talkback braille keyboard.
At least web browsers work well enough
which let's you use web applications.
Unfortunately termux isn't usable by talkback,
so it's not possible to use terminal applications on android.

### Linux

Linux accessibility is a more complex topic
which probably needs a page on its own.
Linux mint and ubuntu come with orca by default,
which can be activated using `alt-super-s`.
The base system is usually accessible,
all web browsers are to my knowledge,
but other third party applications can be a hit or miss.
Just like on MacOS,
I recommend using a terminal screen reader instead of orca,
again, [tdsr][] is the one I use,
but [yasr][] and [fenrir][] work too.

[yasr]: https://github.com/mgorse/yasr
[fenrir]: https://github.com/chrys87/fenrir

### Web applications

Many applications today aren't tied to one specific operating system,
because they all run inside a web browser[^bloat]
which itself is cross platform.

[^bloat]: which is as, if not more, complex than an actual operating system.

Since most web applications are text with some links and buttons thrown in,
screen readers handle them pretty well.
Every screen reader has a way to navigate through the web page,
usually by headings, links, and plain text.
The exact shortcuts differ,
but they are usually a single letter
identifying the element which to go to next
and pressing shift along with the letter
goes to the previous instance of that element.

It's also possible to use the common shortcuts
to navigate through text on the web page.
One that often comes useful is find text
which quickly let's you find a specific part of a webpage
if it's not easy to reach otherwise.

Some very big applications
like google's office suite and office 365
have their own issues with accessibility.
In particular the former needs the user
to press a button to activate accessibility mode.

## Conclusion

This article is a work in progress,
I know it's not very clearly written
and should probably have more information
on how to use it after turning on the screen reader.

However, I hope that it has at least given you an idea of how they work,
and that it has given you an idea of which one to pick.
