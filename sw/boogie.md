# My raspberry pi, boogie

A few days ago I got my raspberry pi 5,
something I wanted to get for quite a while
but only managed to order it last month.
a dozen holidays and enough hell
dealing with customs for a lifetime
it's finally here and I'm ready to use it.

I decided to indulge myself,
bought the 8gb model, a 512gb sd card,
all of which came out to about $250 spent on just one birthday.
Apparently it's not so much these days,
but I still feel somewhat bad about it.

On a much brighter note, it works as expected,
and I've already managed to set up a few things.
Right now I'm using it to host my dotfiles
(no, I'm not going to put them on codeberg)
as well as my [password store](https://www.passwordstore.org/)
This will make it much easier to synchronise my two laptops,
and whatever other pc might come in the future.

I've also set up nginx with php,
and will try to experiment with other web servers as well.
don't like how you need an internet connection to access
documentation for nginx,
and how arbitrarily it divides up things.
Unfortunately other webservers aren't much better,
with the exception of OpenBSD httpd
and hiwatha, and maybe bozohttpd.

I decided to call it "boogie",
at the time because I hated computers
and boogie is a perfectly hateable name.
But only after a few days I've started to like him
so it didn't work as well.
It's also pretty short,
although I hate how I have to constantly put .local everywhere.

I'll see what more I'lll do with boogie in the future,
I might write posts aout it as well.
This is just a list of ideas for me,
and potentially for you if you get a raspberry pi.

* Use it to build and deploy my website to neocities,
  so that I don't pointlessly regenerate things on my various computers.
  Could use codeberg CI for this,
  but I don't ever trust someone else's computer.
* Try out different web servers.
  Will likely start with lighttpd and hiawatha,
  maybe even [naviserver](https://github.com/naviserver-project/naviserver)
  when I'm confident in its documentation.
* Self hos some fun stuff on it,
  although that would only be available from my network.
  Perhaps I bug my ISP to give me a static ip?
* Use it as a notetaker
  I'll need some kind of powerbank first, which is a way into the future.
  Then I'll need either a braille display or speakers, more likely the latter.
  and finally I don't think a notetaker would be that useful.
* Do some hard core math calculations on it
  Maybe get some kind of llm on it?
  a seed box?
  Some kind of mathematical xperiments?
  I better use its ram to the fullest.
