# Why I don't use mastodon

I left twitter some years ago,
pretty much unintentionally.
I used to use [TW Blue][] on windows,
but when I finally switched to my MacBook
I didn't find a comparable alternative.
Instead of settling for worse options I just stopped tweeting.
I didn't use "mass social media"[^mass] since
and I can't say I miss it.

[TW Blue]: https://twblue.mcvsoftware.com/
[^mass]: Here I go making up terms again.
  I'm here referring specifically to social networks
  where you're meant to interact with the whole world.
  This is in contrast to instant messaging platforms and forums,
  which are very different in use and scope.

Any time drama rocked the twitter (now X) boat,
there would be calls to leave for alternative platforms,
most commonly [mastodon][],
And the many people I used to follow ontwitter answered.
I myself gave the various federated platforms a try a few times,
but because of the struggles of finding a good client
and various other personal reasons
I never picked it up like I picked up twitter.

[mastodon]: https://mastodon.social/

I still occasionally check out various people on mastodon out of curiosity,
but I struggle to find a reason to
since most of their content are boosts,
just someone else's message repeated once more.
And those messages are no different to your average tweet.

Mastodon is a good twitter alternative
in the same way pepsy is a good alternative to Coca-Cola.
It's nearly identicle in its usage,
some specifics are nominally different,
and now they even have the same people.
It's amazing if you just want more people on mastodon,
but if your goal was to make a better social network than twitter,
then you just made another twitter.

And that's a problem, because twitter is a bad social network.
It's not bad because the left or the right people are on it,
because of the bots,
or because it's owned by the CEO of Tesla Motors
who really likes the 24th letter of the latin alphabet.
It's because its design encourages bad communication.
There isn't much room in 280 characters to make a good argument
or give all the details and context.
The reader can either spread the tweet further,
or respond to it with another tweet,
facing the same trouble as the original author.
That means that most "content" on such a network
is either meaningless empty platitudes,
or vaguely political ragebate,
either of which is only meant to inspire more tweets and retweets.

You won't find well-argued discussions
or detailed articles on twitter,
at best you could find a link to such a thing.[^bestuse]
Even with a bigger character limit
mastodon encourages the same patterns
and it has the same negative effects.

[^bestuse]: Posting quick little updates,
  telling people about new things you did,
  might be the only good use for twitter and its ilk.

Mastodon, and more broadly the [fediverse][], doesn't even solve
the technological problems of twitter.
The user still has to make an account on someone else's server to access it
and thanks to [fediblock][][^fediblockpage]
some instances are far more equal than others.
In the end, the data itself is still out of the user's control,
what they see is heavily defined by where they happened to sign up,
and the whole stack is far more complexed than a traditional website
And a few choice standards.

[fediverse]: https://en.wikipedia.org/wiki/Fediverse
[fediblock]: https://joinfediverse.wiki/FediBlock
[^fediblockpage]: I can't help but mention
  That the page I linked feels like it was written by a 13-year-old,
  and a very angry and resentful one at that.

Unfortunately, the things that made twitter
an inevitable part of everyday life,
will now make mastodon
an inevitable part of everyday FOSS life likewise.
Fortunately, mastodon does publish rss feeds,
so I'll probably avoid the worst parts of the experience.
