local title = nil

function Header(el)
	if title or el.level ~= 1 then return end
	title = pandoc.utils.stringify(el)
	return {}
end

function Meta(m)
	if not m.title then m.title = title end
	return m
end
