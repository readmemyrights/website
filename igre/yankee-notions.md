# yankee notion cards

"Yankee notion" was an original card deck
published in 1856 by Thomas W. Strong.
Similar to the more usual
[french / international playing cards](https://en.wikipedia.org/wiki/Standard_52-card_deck)
various games can be played with these cards
and 16 were published along with them.

The deck differs substantially in structure from the standard one.
It has 5 suits of 10 cards each,
and doesn't come with extra cards like the joker.
The ranks are digits from 0 to 9,
and the 5 suits are faces, flags, eagles, stars and shields.

The rules have been included in "Hoyle's Games" by Thomas Frere,
likely to help market them.
Those are the most comprehensive rules that can be found today,
other accounts usually repeat what's said there
in perhaps more readable prose.
The 1875 edition of Hoyle's Games can be
[found here on archive.org](https://archive.org/details/hoylesgamescont01hoylgoog)
and a pdf is also
[archived locally here](hoylesgames1875.pdf).

Besides their inclusion in various editions of "Hoyle's Games"
from the 1850s to the 1880s,
these cards very rarely get mentioned,
and almost exclusively by games researchers at that.
Perhaps the most well-known mention
is in Sid Sackson's "A gammut of games" pages 26-27,
where he only discussed the structure of the deck
and wrote about one of the games,
specifically [hekaton].
[The wikipedia page for a gamut of games](https://en.wikipedia.org/wiki/A_Gamut_of_Games)
seems to be the only mention of these cards on wikipedia.

They're mentioned in 1890
in a book about the history of cards called
["The devil's picture-books"](https://www.gutenberg.org/files/68367/68367-h/68367-h.htm)
([archive](https://archive.is/loZIr))
(pages 119-121).
There isn't much more information here however,
which suggests that by the end of the 19th century
this deck became a historical curiosity
despite not even being around for 40 years.

They also get mentioned
[in this article about the joker card](https://hobbylark.com/card-games/The-History-of-the-Joker-Card)
([archive](https://archive.is/DzDiT))
which is noteable for being the only article
that explicitly states Thomas W. Strong was the designer
although none of their sources mention it.
However, it's likely that's the case,
since Thomas W. Strong ran
[a successful humor magazine by the same name](https://graphicarts.princeton.edu/2015/08/20/yankee-notions/)
([archive](https://archive.is/qolml)),
owned the publishing house that published Frere's hoyle,
and there was a sketch
in the September 1854 edition of Yankee Notions magazine
called "A black joke"
which is a name of one of the games,
although it's unclear what they have in common other than the name.

## Terminology and definitions

In this article
references to games, cards, suits, ranks, etc
refers to those of the yankee notion cards
unless specified otherwise.

The 52-card deck
will be called "standard" throughout most of the text,
for lack of a nicer-sounding term.
There's nothing "standard" about it however,
it's just very commonly used.

"The book" refers to the 1875 edition of Thomas Frere's Hoyle's games.

Most games played with these cards
are adaptations or were otherwise inspired
by games played with the 52-card deck
in 19th century America,
and this description reuses as many terms from them as applicable.

### The deck

The suits are divided into two _classes_,
"figures" or _pip_ cards,
comprising the suits of flags, eagles, stars and shields,
and the _face_ cards,
comprising the suit of faces.

There are 10 _ranks_ in each suit
from 0 to 9.
they nearly always count face value,
with the exception of 0
which sometimes counts as 10.
The book often calls 0s "graces",
especially of pip suits.

#### Faces

There are no face ranks as in standard playing cards,
but instead the face suit
features various characters on its cards.
Their names are as follows:

0. John Smith
1. Mrs Sally Smith
2. The Baby
3. An Old Maid
4. An Old Bachelor
5. Sweet 17, "ready for an offer"[^sweet17sic]
6. Parson, "also ready for duty"
7. Ruth, "the Quakeress"
8. Ezekiel, "Ruth's Husband"
9. The Watchman

I'll refer to most of these by their rank number,
with the exception of John and Sally Smith
when they have special significance.

[^sweet17sic]: The text in quotes after the card name
  appear in the book but aren't mentioned again.
    They were probably meant to describe cards further.

#### Card notation

I'll refer to the cards as
_rank_ of _suit_,
for example nine of flags,
zero of shields,
5 of faces.
To be brief,
sometimes I'll use a shorthand _rs_
where _r_ is the rank, as 0, 1, ..., 9,
and_s_ is a letter identifying the suit, as follows:

* *F*: faces
* *B*: flags (banners)
* *E*: eagles
* *\**: stars
* *S*: shields.
* *P*: any pip.

The two-character notation is my own.
The book usually uses the full names,
the 0s are usually called Z's.

#### Suit pairs

In the game of [pick-nick]
there's a system of pairing suits
similar to the colors of standard playing cards.
One pair is flags and stars,
while the other is shields and eagles.
Since I couldn't find any pictures of these cards
I don't know if they shared color.
Note that faces aren't in either pair.
This makes sense in the context of pick-nick,
but they can't be adequately
used in, say, an adaptation of Klondike.
I'll talk more about this later.

### Deal and Play

All games are played clockwise,
and the deal, if not otherwise decided, is decided by lowest card,
0s being low.
The player on the right of the dealer may cut,
and the player to the left begins play
and is called the elder hand.

### Notes on trick-taking games

During the deal, the first card delt to the dealer is face-up.
If it's a pip,
then it's suit is the trump suit.
if it's a face, the second card to the dealer is face-up again
and so on until a trump suit is picked
at which point the dealing continues
without any face-up cards.

In a 4-player game
there are 2 cards left over at the end of the deal.
The first of these should be dealt to elder hand, and the last to the dealer.
They should have one card more than the other players
and at the end they'll play out the last trick
between each other.
Whose team won the second-to-last trick
will play the first card.

The leader of the trick may play any card.
other players in clockwise order must play as follows:

: If the lead suit is a pip suit

  They must follow suit if they're able, otherwise may play any card.

: If the lead suit is faces

  They must play a face if they have one,
  otherwise they must play a trump,
  and if they have neither they may play any card.

: If the lead suit is the trump suit

  They must play trump if they have one,
  otherwise they must play a face,
  and if they have neither they may play any card.

The winner of the trick is decided as follows (0s high):

* The highest card in the trump suit
* If there are no trumps, then the highest face
* If there are neither, the highest card of the lead suit.

A few examples to make the above rules clearer.
Elder hand leads in all examples,
and eagles are trumps:

* 0S, 1S, 4S, 5S:
    Everyone followed the lead suit of shields.
    The leader of the trick
    wins with their zero of shields.
* 9S, 3S, 1B, 1F:
  Leader lead shields again, and dealer's partner had shields
  so he had to play them.
  Leader's partner doesn't have shields,
  but he doesn't want to take a trick from his partner so he throws off.
  The dealer doesn't have shields either however, so he cuts with a face.
  The dealer wins the trick and would lead the next one.
* 0F, 2F, 3F, 4F:
  The leader leads the face suit.
  Since everyone had faces they had to play them.
* 0F, 2F, 1E, 4F:
  Leader leads faces, but his partner doesn't have any.
  He must "cut" over his partner
  even though he wouldn't probably want to in this scenario.
  Leader's partner wins the trick.
* 0E, 1E, 5E, 2F:
  Leader leads the trump suit,
  second and third player had eagles so they had to play them.
  The dealer didn't have eagles but had faces so he had to played them.
  Leader wins the trick.

NOte that the faces are still a separate suit,
they aren't just an extension of the trump suit
like the queens and jack's in [sheepshead](https://www.sheepshead.org/).

Note that the suits are shorter than in the standard deck.
While 0s are sure winners,
9s are quite risky especially in a longer suit.
Faces on their own aren't sure winners,
especially when cutting.
If you want to get a sure trick out of 0F
play it as soon as possible,
before nobody voids faces by cutting.

## The games

I'll go through the 16 games
as mentioned in the book,
retelling the rules
and adding comments if I see fit.

The descriptions here shouldn't contradict the book
just say them in a hopefully clearer way.
I'm not aware of any current players of these games,
if there are any I'd love to
[hear from them](/contact.html)
about the way they play them.

I'll describe the games accurately,
and later discuss my suggested changes,
that I'll clearly note are not official rules.

### Trick-taking games

Some of the games are trick-taking games,
most similar to whist in play and scoring,
with the exception of mum
which is an adaptation of 66.

Unless otherwise specified,
[notes on trick-taking games]
apply to all games in this category.

#### Tilters

2, 3, or 4 people may play,
and according to the book it's best with 2 or 2 teams of 2.

The goal is to be the first to reach the target score.
When two play, the target is 10 points,
When 3 it's 15,
and with 4 it's 20.

Deal 13 cards to each player,
unless there are 4,
in which case each gets 12
and the last card goes to elder and dealer,
as described [above][ttg].

[ttg]: #notes-on-trick-taking-games

Play starts from elder hand
and continues clockwise.
Each player plays a card to the trick,
Subject to [the rules of following suit][ttg].
The winner of the trick leads the next one.

To score for tricks,
the side must have at least a certain number of tricks.
When 2 or 2 teams play it's more than 6,
when 3 play it's more than 4.
Each trick over counts a point.

In addition, each face (tilter) captured counts a point
regardless of its number.
However, a side can't win by just capturing faces,
they must win by tricks.

This can introduce confusion.
For example, if a team has 16 points,
won 3 points from tricks and 5 from tilters,
how many points would they have?
Given that the book mentions
"having only one point to make on a new deal"
it's likely in the spirit of the game
that the tricks get counted first,
and if they haven't crossed the target score
then the tilters are counted
unless they would count over the target
in which case they don't.

On average,
much more points would be won on tilters than on tricks.
That's why I suggest scoring tilters similar to tricks,
1 each over half,
so the side that captured most of them
would score number of faces minus 5.

#### John Smith

Played by 4 players in 2 teams of 2.

The object of the game is to score 20 points,
as follows:

: 0F (John Smith)

  5 points either saved or won (see below)

: Tricks

  7 tricks score 5 points,
  each trick over 7 is worth 1 extra.
  For example, 8 tricks are worth 6.

: Faces

  The side that counted the most faces by addition scores 5.
  The sum total of all faces is 45,
  ($`0 + 1 + \dots + 9 = 45`),
  and the side that captured 23 or more wins 5 points.
  the number of actual cards doesn't matter.
: 
 Graces

  Each pip 0 captured counts 1 points.

Deal and play are the same as in tilters.
With the following exception:
if the 0 of faces (John Smith)
and the 1 of faces (Sally Smith)
are played in the same trick,
then the player who played the 1 wins the trick,
regardless of trumps or other faces,
and he wins 5 points for John Smith.
If this doesn't occur
then the holder of the 0 of faces
wins the 5 points
regardless of who captures it via trumps.

As described by the book (emphesis and footnote are mine):

> Mrs. Smith (Sally, John's wife), is the faced ace,
> and always captivates and takes poor John against all opposition whenever she falls in the same trick with him.
> No trump can prevent her securing her prize.
> In her husband's presence (in the same trick) she is superior to John and all other cards,
> and takes the trick.
> Separate from him she is the lowest and humblest of the Faces.
> On the contrary, John in her presence is nobody, but away from her he is the highest Face, or A _No. 1_.[^not1]

[^not1]: To dispell any confusion, in a trick without Sally Smith
  John Smith isn't of rank 1, or indeed the highest card at all,
  because a trump card is higher than any face.
  This seriously tripped me up the first few times
  I read the description.

Further, on his lead the holder of the 1 of faces
can call for the 0 of faces (if he doesn't hold him himself)
By saying the following:

> Come forth, Great John  
> Thou Paragon!  
> My voice I'm sure you know!

In response,
the player holding the 0
must play it
and may say the following:

> I know that voice!  
> I've got no choice!  
> It's hard, but I must go!

Whoever reaches the target score first wins the game,
counted in the order
John Smith, tricks, faces, graces.

The target score is way too low,
I recommend a target score of at least 40 points.
I also recommend changing the scoring schedule as follows:

* John Smith: 5 points if captured by 1 of faces,
  otherwise 2 to whoever captures him, not necessarily the holder.
* each trick  over 6: 1 each.
* Faces: 3
* Graces: either 1 each, or don't get counted at all. They are very easy to get,
  and are usually worth 1 trick on their own.

For the holder of 0F, the goal is to play
and win the trick with it,
without falling to the holder of 1F
Likewise, the holder of 1F wants to get the lead
and call for the 0F.
The other players should focus most on tricks and winning faces.
The 9F could be a good lead,
hoping that the next to players hold 0F and are too afraid to play it
and give away their faces for the face score.

#### Pick-Nick

This game may be played by from 2 to 6 players,
but the book focuses on the 4 player version with partnerships.

This game uses a reduced deck,
including all the faces
but only 1, 7, 8, 9 and 0 of pip suits.
This makes for a total of 30 cards.

The order of cards differs among trump and non-trump suits.
The trump suit is ordered as follows:

* The one of trumps, or the champ.
* The other ace of the same color as the trump suit,
  as described in [suit pairs].
  called the Filly or fillibuster.
* the 0, 9, 8, and 7 of trumps.

Other suits go as usual from 0 to 1,
but one suit wouldn't have its one.
The filly is the part of the trump suit
and not the suit on it.
Thus, if the filly is the only trump in your hand
you must play it to a trump lead.
And you can't use it
to follow its original suit.

Each player is dealt 5 cards,
neither or elder nor dealer getting extra like in other games.
The next card afterwards is flipped up.
If it's a face,
flip up another one over it
until there's a pip-suited card on top.
This is the proposed trump suit.
If there were any faces flipped up,
the dealer may take them
and discard an equal number of cards in return.

Each player in turn
starting from elder
may either accept the proposed trump suit or pass.
Traditionally elder hand, his partner, or the dealer would say
"I order it up"
while the dealer's partner would say
"I assist"
to accept the proposed trump suit.
There's no other difference in procedure.

If all players pass
the proposed trump card is turned down
and now each player may either propose a different trump suit or pass.
If all players pass again
the hand is thrown in
and the cards are shuffled and dealt again.

Once someone has accepted a trump suit,
whether the proposed or another one,
their team become the declarers
and the other team become the defenders.

The player who accepted the trump suit
may decide to "go alone"
before play begins.
His partner doesn't participate in the game
only the declarer and two opponents.

Play as in other trick-taking games,
starting from elder hand.

At the end of the round
the declarers count the number of tricks they took,
and score as follows:

* If they took 3 tricks,
  they get the pick, and score a point.
* If they took 4 tricks,
  they get the nick, and score 2 points.
* If they took all 5 tricks,
  they get the Pick-Nick, and win 3 points.
* If they lose 3, 4, or 5 tricks
  they get picked, nicked, or pick-nicked respectively,
  and the opponents score 3, 4, or 5 points.

The points for getting pick, nick or pick-nick are increased by 2
when the declarer goes alone.

Game is 7 points,
but 10 might make for a more comfortable game.

This game is an obvious adaptation of euchre
with more scoring opportunities
but also harsher punishments.
Euchre players should keep the following in mind, however.

* Even though they technically aren't trumps,
  faces can be used to cut,
  and more importantly comprise the third of the whole deck.
* While pip suits (except trump) are just 1 card shorter than in euchre,
  a single suit is only a sixth of the deck,
  as oppose to euchre's forth (assuming a 24-card deck).
* From this follows that 0s aren't very reliable winners,
  especially when they aren't singletons.

For those who think that punishments
for getting picked, nicked, and pick-nicked are too harsh,
reducing points given to the opponents by 1
returns it to something more similar to traditional euchre.

#### Mum

This is exclusively a 2-player game.

The objective in a round is to score 60 points by capturing cards
and calling it out when appropriate.

This game also uses a reduced deck,
containing all the faces
and the 0 through 3 of other suits,
making 26 cards.

Deal each player 6 cards
and put the remaining deck between them.
The faces are always and only trumps,
no card is flipped up for trumps.
The rest of the deck is called the stock.

Each player plays a card into the trick.
As long as there are cards in the stock
there is no need to follow suit or play a face.
The highest face wins the trick,
if there are no faces
the highest card of the lead suit wins the trick.
The winner of the trick draws a card from stock,
followed by the loser,
and then The winner of the trick leads the next one.

Each card counts face value,
0s counting as 10.
In addition, pairs of faces
adding up to 11,
like zero (worth 10) and one,
9 and 2, etc,
can be shown and scored
when their holder has the lead
and has won at least one trick
by leading one of the pair
and showing the other.
Each pair counts 20,
except for the pair of zero and one
which counts 30.
Players may not look back at their captured tricks,
they're supposed to keep the points they scored in their heads.

WHenever the stock runs out of cards,
players continue playing.
The leader may lead any card,
but the follower must:

* Follow suit,
  and if the lead suit is a face beat it if possible.
* If a non-face is lead,
  and the player can't follow he must play a face.
* If he can't follow any of these rules he may play any card.

Any time both players have an equal number of cards
either of them can wrap by knocking.
The stock is put to the side,
and the game continues as if it just run out.

A game ends either when all cards are played out,
or more likely when a player calls 60.
If the player who called 60 has 60 or more points
in captured cards and declared pairs
he scores a game point if his opponent had 30 points or more,
2 game points if the opponent had less than 30 points
but won at least 1 trick,
and 3 game points if he didn't take a single trick.
If the caller didn't have 60 points however
the opponent scores 2 game points.
If nobody called 60 before all cards are played out,
procedure goes as if the player with more points called at that point.
Play until 7 game points are scored.

This game is clearly an adaptation of 66,
however players of 66 should keep the following differences in mind:

* The trump suit is much longer than the others,
  and is all together worth over 3 times more than one plain suit.
* Every card is worth at least 1 point,
  so just winning a trick is worth at least 2 points.
* All trump cards participate in on potential marriage,
  one should be careful when cutting.
* The deck is all together worth
  119 points,
  thus 60 is just over the half,
  and it's impossible for neither player to reach 60.
* There's no 10 points for last trick.
* There's nothing equivalent to exchanging the nine,
  which reveals less information about the contents of the deck
  and the contents of players' hands.

The book calls faces "teasers".

The game description doesn't make any direct references to 66,
but it does say its of German origin.
The author of the book is clearly aware of 66 however,
since the book includes its own description of the game
as well as the following passage in the preface
(emphesis and footnote are mine):

> The German game of "_sechsundsechszig_,"[^ocrerror] or Sixty six, has never before,
> that we are aware of, been dressed in an English garb. We do not hesitate to
> pronounce it the best game of cards for two players that we ever practised.

how this adaptation got the name "mum"
I'm not sure. It doesn't appear anywhere within its description.

[^ocrerror]: That's at least what I think it says,
  that's what the game is called in German.
  OCR is having a hard time with this book.

### Gambling games

The games in this category
are played with hard score,
physical counters that are given from player to player
over the course of a session.
The counters could be coins,
[chips](https://en.wikipedia.org/wiki/Glossary_of_card_game_terms#chip),
or something utterly worthless.

These games aren't all just games of chance,
and as mentioned above don't need to be played for money.
However the account in the book does imply it was meant to be,
and some of the games are similar to actual casino games.

In the following descriptions I'll use the word "counter"
to refer to the unit being played for,
which doesn't need to be a single physical token.

#### Quien sabe

Playable by up to 8,
but probably best with 3 to 5.

Each player puts a counter into the pot,
and the dealer deals them 6 cards in batches of 3.

Each player in turn,
starting from elder hand,
plays one card face up.
After everyone has played a card face up,
subsequent cards are played face down.

The goal is to call out "Quien sabe"
when all cards played,
face up and face down,
are greater but as close to the quien sabe number.
The quien sabe number (from now on qsn) is equal to 10 times the number of players in the game,
so 20 when there are 2,
30 when there are 3,
and so on.

The player who played the last card
must either call out "Quien sabe" or "I pass".
Each player in turn after him
has the same choice,
and only once everyone has passed
may the next player play another card.

Nobody can call out "Quien sabe" on the first round,
when all cards are face up.
If the sum of the face up cards
comes to within 10 of the qsn
before all face up cards are played,
the hand is redealt.

When a player calls out quien sabe,
all cards are turned face up
and somed together,
0s being worth 10.

* If the sum is less than the qsn,
  the caller must pay each player the difference between the qsn and the sum.
  The pot remains for the next round.
* If the sum is greater than the qsn,
  each player pays the caller the difference,
  and the caller takes the pot.
* If the sum is equal to the qsn,
  the caller wins the pot,
  and each player pays the caller
  double, tripple or even quadruple the contents of the pot,
  as previously agreed.

This is a simple game,
which while giving a fair bit of choice to the player
doesn't give himenough information to make a good one.
There are two conflicting objectives:
calling the qsn exactly,
or calling when the sum is the highest
for the greatest reward.

It's all too easy to spoil the game
by always playing so that the sum of the shown cards
gets within 10 of the qsn,
especially when in conspiracy with another player.

The book suggests the game can be played with soft score,
each player summing their winnings with pen and paper
and the first player to reach 100 points winning the game.

It's possible to increase the ante
or the rate at which differences are paid out.

#### Bunkum

Playable by 2 to 4 people,
each for themselves.

The objective is to win the pot
by holding the lowest hand after discarding fifteens.

The dealing schedule is as follows:

* 2 players: 10 cards to each player,
  and 5 in the middle of the table.
  The book calls these "zillls" or "zilias",
  here I'll refer to them as the reserve.
* 3 players: 10 to each player and 7 to the reserve.
* 4 players: 10 to each player and 10 to the reserve.
  The book suggests dealing to 5 piles in a circle,
  1 at a time.
  the pile on which the last card falls is the reserve,
  the one before is dealer's hand, and so on.

After dealing,
there's an auction for each card in the reserve.
each player, starting from elder,
may either bid or pass.
Each bid must be higher than the last,
and a player who has passed may not rejoin the bidding for the same card,
but he may bid in the auction for the next card.
Once everyone except 1 has passed,
the bidder pays the amount of his bid into the pot
and draws one card from the top of the reserve.
If everybody passes,
the reserve is discarded
and the game continues to the showdown.
Likewise once the whole reserved has been sold.

Once the bidding is over
but before the showdown
players may make side bets among themselves.
These bets should be kept separate from the pot,
since the winner of the pot doesn't necessarily have anything to do with them.
The book doesn't go over what kinds of side bets are allowed,
but I suggest not allowing players to make bets about themselves
or bets on who will not win the pot,
but bets on who has the highest hand is of course allowed.

Finally, each player in turn may discard one or more cards
so that each batch when summed up is equal to 15.
A card can't be a part of more than one batch.
Zeros count 0,
while they can't participate in 15s
they don't hurt the total
and in fact help in numerical ties,
(see below).
Once a player has made his discards
they are taken away and they are out of the game.

The player with the lowest hand, by addition, wins the pot,
and each player must pay him the difference between their total and his.
At this point all side bets are appropriately handled.
If there are numerical ties for the lowest hand,
it's broken as follows:

* If some of the hands have 0s in them,
  the card with the highest 0 wins,
  ranked in the order faces, flags, eagles, stars, shields.
* Otherwise, the hand with the highest suit,
  as per the above ranking wins.
  if they ty for highest suit,
  the hand with the highest rank
  within the highest suit wins.
Again, these rules only apply
when there are numerical ties.

#### Black Joke

This is a banking game.
One player is the banker,
who handles the bets and dealing,
and other players "the punters".
There can be various amount of punters,
but after 10 it might get uncomfortable with a single deck.

Each player in turn buys cards from the banker
at a rate of his choosing.
The banker takes the payment,
deals the player his cards face down,
and once the first round of orders is complete
players may buy additional cards
at the same rate,
as long as there are any left in the deck.

Once all the orders are complete,
each player turns their cards over
and get paid for each face they hold.
For each face the banker pays the player
the number on the face times the rate at which it was bought.
The zero of faces counts as 0,
so it's as good as any pip card.
In this game,faces are called jokers
and a hand without jokers is called a black joke.

This game is 10% in the banker's favor,
if a player buys all 50 cards at rate $`x`,
he would win $`(1 + 2 + \dots + 9)*x = 45x`
which is $`5x` less than the amount given to the banker.
Removing 0s makes it an even game.

#### Jockey club

This game can be played by any number of people,
but at most 5 can play comfortably with a single deck.

Remove the zeros from the deck,
and afterwards deal $`n+1` cards to each player
where $`n` is the number of players.
Thus 3 cards are dealt to each player when 2 play,
4 when 3,
and so on.
Turn over the next card face up.

The object of the game is to win the pot
by holding a hand
whose total is equal to 5 times the number of cards in it,
or in other words whose average is equal to 5.
If nobody holds such a hand,
they might improve it by getting the turn up.

The turn up can be gained in the following ways:

* Bidding: like in the game of bunkum,
  a player may bid a price at which he'd buy the turn up.
  each player must bid higher than the others,
  and once he has passed may not rejoin the bidding.
  The bid is paid into the pot.
* Consoling: instead of bidding,
  a player may bid an amount he's willing to take
  to pick up the turn up.
  The consolation is paid out from the pot.
  each player must console for a lower amount than the previous player,
  and consolation should start at the amount initially paid by the players into the pot.

Elder hands starts by either bidding or consoling.
Once he has consoled, he may not bid later
but other players may join in consoling
and afterwards "turn the corner" and bid.

Bluffing in this game can be quite profitable,
either consoling for a card they want,
or participating in the bidding
to make others pay more.
There's a danger of others calling his bluff,
forcing him to take a card he doesn't want,
perhaps at a quite hefty price.
The book calls bluffing "jockeying"
and failing to jockey is called "laming".

If multiple people claim the pot,
the winner is decided as when breaking ties
in the game of bunkum.

It's unclear how exactly bidding procedure is supposed to go,
and with possibility of bluffing
it's hard to come up with a good system
without proper playtesting.
here's a potential suggestion:

* First, each player may bid for the turn up,
  as in bunkum.
* If everybody passes,
  then players may console.
  If everybody passes again,
  the dealer must pick up the turn up for free.

The dealer has an advantage in this system,
if he wants the card and nobody else does,
he only needs to pass during bidding
and then console for nothing and get the card for free.
Only way to fully solve it
is to forbid the dealer from consoling.
The advantage won't apply to the player on his right,
because they can't be sure the dealer would pass too.
This is all just theory for now.

5s make no difference to the scoring,
and may be either buried when they're turned up,
or thrown out of the deck entirely.

It's unclear how the pot is built,
but each player paying in a suitable amount before the game
would ensure that consoling can be supported with appropriate limits.
Thus, the entry should be a multiple of the unit that is played for.

### Adding and matching games

This broad group of games
involves pairing, summing, or otherwise grouping cards
usually by their numerical value.
There are cards on the table
that can be taken by players with their own cards,
similar to [fishing games](https://www.pagat.com/fishing/).
A noteable exception to this format is [cribbage]
which is an adaptation of the standard deck game
which defies all attempts of classification.

The book often calls the cards on the table a "trick",
even though they don't function
like tricks in [trick-taking games].
Here I'll call those cards the talon.

#### Saratoga

4 players play in 2 teams of 2.

Deal 12 cards to each player singly,
and turn the remaining cards face up
to form a talon.

The goal is to capture cards from the talon
by either adding them up to a multiple of 10,
or capturing pairs of 0 and 5 called pents.

On his turn, a player plays a card,
and if he can complete a capture with it
and other cards from the talon
he (or his partner) puts them in their teams capture pile face-down.
The possible combinations are as follows:

: Multiple of 10

  a group of cards of the same class,
  either all pips or all faces,
  can be taken together if they add up to 10.
  If they are faces,
  then the 6, 7, 8 or 9 in the combination scores face value.
  Otherwise the team capturing them scores the sum of the cards divided by 10.
  0s and 5s don't participate in this combination.

: Pents

  a 0 and 5 of pips, not necessarily of the same suit,
  is a pent and scores 5 points.

: Bipent

  The 0 and 5 of faces comprise a bipent
  and is worth 10 points.

If a player can't capture any cards
he must leave the played card on the table
where it can participate in future captures.

Game continues until all cards are played.
The last player should be able to capture the remaining cards
with the one he has left,
if he can't there's been an error in the game,
and the side that made it should lose.

The total points in a game is 76,
and the side that captured the most wins the game.
If both sides captured 38 points
the side that captured the bipent wins.

Strategies common to standard fishing games apply here.
In particular, making the total on the table equal 15
can be used to make sure
there's at least one card for your partner to take,
since the opponent can't use a 5 to capture them both.
Keeping track of play cards is very profitable,
as well as moves not made.
If a player didn't capture a 5,
then he likely doesn't have any 0s,
and similar for other cards.

The scoring schedule is unbalanced
in favor of the pents and the faces.
Here are a few potential changes:

* Each pip 10 scores 2 points instead of 1.
  This brings the total number of points to 92,
  32 of which are pip 10s,
  30 of which are faces,
  and 30 of which are the pents.
* instead of faces counted the number on them,
  score them the same as the pips.
  This makes the total 50,
  over half of which are pents.

The game doesn't discuss a possible,
although very unlikely scenario,
if the starting two cards of the talon
are the 0 and 5 of faces.
The most fair approach is to declare a misdeal
and give the cards a good shuffle,
but considering the chances of this happening are $`2/(50!)`
(where $`50! \approx 3.041409320 * 10^{64}`)
I suggest doing something to celebrate the occasion.

The book doesn't mention versions for other numbers of players,
but it's easy to come up with appropriate dealing schedules.
Some I suggest:

: Two players

  Deal each player 12 cards and 2 in the center.
  Once all 12 cards are played deal another 12,
  which should be the rest of the deck.
  It's also possible to deal 6 at a time.

: Three players

  Deal each player 16 cards and 2 to the table.
  Or alternatively deal 4 or 8 cards at a time.

: Four players

  Besides dealing 12 out at once,
  it's possible to deal 6 at a time,
  which reduces scope for skillful play but makes it easier for beginners.

#### Chance 10

Any number of people can play this game,
each for themselves.

Each player cuts and draws a card from the deck,
lowest card begins the game (0 being low).

The first player shuffles,
the player to the right cuts,
and then each player pays into the pot an equal amount.

The first player starts by drawing a card and placing it on the table.
The player next in clockwise order draws a card,
and if he can match with the card on the table
as in [saratoga]
he captures the cards
and draws another one.

In general, a player's turn
starts with them drawing a card.
If they can capture another card they capture them and draw again,
otherwise they put it on the table,
ending their turn.
The player on their left does the same.

Note that only one card can be captured at a time,
so it's not possible to score 2 tens at once
by summing multiple cards.

Game continues until the cards run out,
which should come out exactly as in Saratoga.
Who captured the most cards wins the pot.

As its name says
this game is of pure chance,
and is little more than a betting game.
It's only because of its similarity to Saratoga
it's in this section, for now.

It might be useful
as a more elaborate and time-consuming way
of picking a random person from a group,
as a less deterministic [counting-out song](https://en.wikipedia.org/wiki/Counting-out_game).

Suggestions in the scoring schedule I mentioned for Saratoga
apply here too.

#### Double couples

This game can be played by any number of players,
but the maximum is 8.

Each player is dealt 6 cards,
and two cards are delt onto the table
separate from each other.
These cards are called "leaders".
The remaining cards are put where all players can reach them face down
and are called the stock.

The dealer plays first and the game continues clockwise.
At his turn, a player matches one of the leaders
with a card of the same rank from his hand
and puts them both face up in front of him.
Afterwards he replaces the taken leader
by another card from his hand.
If he can't match either leader
he must draw from the stock.
If the stock runs out of cards
he must pass.

Play continues until a player runs out of cards
or it's impossible to match either leader
because two couples of the same rank were already matched.
Leaders that can't be matched are called blockers

The objective of the game is to play out all the cards,
or to have the lowest sum of cards
when the game gets blocked.
When summing cards, 0 count as 0
and ties are decided as in [bunkum].
However, if a player plays out all his cards
he wins regardless of other player's sums.

For some reason the book
calls the sum of card values "peccadils" or "peccadillos"
which may either mean "insult" or flaps on medieval clothing.
In this context it probably refers to pips,
but this term isn't used anywhere else in the book.

The descriptions says that the couples should be kept face up
and clearly visible throughout the game,
probably to make it clear when the game is blocked.
For a harder game I suggest keeping the couples face down,
and require players to call out when the game is blocked.

The book describes this as a gambling game,
but it can be played effectively for points tooo.
Either giving the winner of the round
the sum of other player's cards minus their own,
or each player getting penalty points for the cards they hold.
The player that reaches a score limit
is the winner or is out of the game respectively.

#### Triplets

Four players in 2 teams of 2 can play this game.

This game is played on a 3x3 grid of cards,
one for each ran besides 0s.
Each cell in the grid is for a specific rank,
as follows:

| 2 | 9 | 4 |
| 7 | 5 | 3 |
| 6 | 1 | 8 |

It's best to draw the layout
on a sheet of paper
and play cards onto it
to avoid lapses.

The objective of the game
is to capture triplets
by filling a row or a column of the layout
so that it equals 15.

Each player is dealt 12 cards,
and the last two cards are placed
on their appropriate place in the layout.
If any of them are 0s
they are to be taken by the dealer.

Play starts from elder hand.
Each player on his turn must play one card
by placing it on its appropriate place in the layout.
If they fill a row of column
that consists entirely of face or entirely of pip cards
they take the triplet
and score a point if it consists of pip cards,
3 points if it consists of face cards.
It's possible to play a card on top of another,
in which case they should be fanned out so both are visible,
and either can be taken in a triplet,
but of course not both.
If a player plays a 0
he shows it and captures it,
effectively passing his turn.
Each 0 of pips is worth 1 point, and the 0 of faces is worth 2.

The dealer should be able to capture the remaining triplet with his last card,
unless it's a 0, of course.
If not there was an error.

There are 27 points in a single round,
12 for the pip triplets,
9 for the face triplets,
and 6 for the 0s.
Whoever scores the most points wins the game.
Suggestion: each team gets the points they scored
and more rounds are played until someone reaches 100 points.
To reduce the luck element
0s shouldn't score any points,
but can be still played as described above.

This game has a fair bit of scope for skillful play.
and you won't go far if you don't pay attention to what's played,
and possible captures not just for yourself,
but for your partner and opponents.

If the dealer keeps a non-0 face until the end
he's sure to take at least one face triplet.

A fork or a corner
is when a card
is flanked by two other cards,
one verticly and the other horizontally.
There are two ways to capture the card in the center,
and if it gets captured one way
it can't be captured the other.

If you have two cards of a face triplet
and the third one is on the table
then chances are good you can take it
if you play cards on after another.
However becareful to not create a fork,
which could let your opponents take one of the cards via another triplet.

#### Robin Hood, 40 thieves

This game is played by 4 players in 2 teams of 2.

First, the 10 faces are taken from the deck,
the 1 of faces is taken out,
the rest are shuffled,
the 1 is place at the bottom of the face deck,
and finally it is placed face down in the middle of the table.
The book calls these cards "victims",
while the rest of the deck are the thieves.

The rest of the deck is shuffled
and 10 cards each are dealt to the players.

The dealer flips the top card of the face deck over.
this card is "the victim"
and its capture value is 4 times its rank,
0 counting as 10.
So the 1 of faces has a capture value of 4,
2F has a capture value of 8,
and 0F has a capture value of 40.

On his turn,
a player plays a card to the table.
If it, together with some other cards on the table,
is equal to the victim's capture value
he takes the victim card
along with the cards used to capture it,
and a new one is flipped up to replace it.
Victims and the thieves used to capture them
should be kept separate
to make it easy to confirm the game was played right.
If he can't make such a sum
the card remains on the table
and can be used in future captures.

For example, it's possible to capture 4F
with 5\*, 0B, and 1S,
$`4\cdot 4 = 5 + 10 + 1`.
It doesn't matter what suit the played cards are,
but they obviously must all be pips.

The turn rotates around the table
until all cards are played out.
If the victim can't be captured
with the dealer's last card
he takes it and all the remaining ones.

A team scores the face value of each victim the capture,
_not_ the value needed to capture it.
If the captured 0F, 3F, 5F and 9F, they score $`10 + 3 + 5 + 9 = 27`.
The team that captured the most points wins the game.
Alternatively points can be added to each team's total
and the first team that reaches 200 points wins.

The reason why 1F is taken out of the victim deck
and put at the bottom of it after shuffling
is to negate the advantage the dealer's team would have
if a high-scoring face was at the bottom.
A simpler way to handle this
is to reward the remaining victims
to the team that captured the last one,
or to not award it to anyone at all.

This is one of a few games
that has an overarching theme,
although fundamentally it's still an adding up game.
Thieves robbing innocent people
is a quite dark and morbid theme,
and feels quite out of place
in a game for a deck supposedly free of sin,
but it doesn't detract from the fact
that this is a fun and educational game.

This game can be adapted
to be played by more or less players.
2 players can get 10 cards,
and another 10 when they finished playing the first.
3 players can get 13 cards each,
and the remaining one used to start the talon.
5 players can get 8 cards each.
Otherwise the rules stay the same.

This game shares one of its names
with a solitaire game [40 thieves](https://en.wikipedia.org/wiki/Napoleon_at_St_Helena)
which is certainly a coincidence,
since they have nothing else in common.
It's interesting to note that
this game got its name before the solitaire did,
and while it's not obvious when the solitaire game 
was first played
it was only described in 1870
while this one appeared 20 years prior.

#### Trikonta

2, 3, or 4 people may play,
4 players in 2 teams of 2.

This is a adding-up game,
a very simple version of [ninety nine](https://www.pagat.com/adders/98.html).
The goal is to either make the total equal to 30,
or to at least play so that the total doesn't go over 30.

Each player is dealt 2 cards,
and the rest is kept somewhere
where all players can reach them,
and it will be referred to as the stock.

Starting from elder hand,
each player in rotation
plays either a card from their hand
or the top card of the stock.
The played card adds its value to the total,
0s being worth 0.
If the player played from his hand
he must draw a card from the stock to replace it.
A player must always have exactly 2 cards in his hand.

If a player can't play either of the cards in his hand
without bringing the total over 30,
and a card from the top of the stock
would bring it over as well,
he's "bursted" and is out of the game,
he doesn't play any more cards
and can't win the round.

If a player makes the total equal 30
or if he's the last one left in
after everyone else has burst,
the round ends
and he wins a point.
The first player to reach 5 points wins.

While this game offers little as far as skills needed
(safe for very basic addition),
it's delightful in its simplicity,
especially compared to other adding games.

It's possible to change the amount of cards
each player holds,
3 or 4 might be best.

Instead of winning points,
each player could start with a number of lives,
losing one whenever they burst.
If a player hits the total exactly,
one of the following could happen:

* Nothing, others only survive
  if they play a 0,
  and otherwise they burst.
* Other players each lose a life.
* The player that made the total equal 30
  gets a life back.

#### Cribbage

2 people can play,
and it's possible with more,
although I personally don't recommend it.

This classic can be easily applied to the yankee notion deck.
The book only mentions the differences that needed to be made,
and I'll do so as well.
[pagat.com has a page on cribbage](https://www.pagat.com/adders/crib6.html),
which does a great job explaining the game
as played with standard playing cards.

* 0s count as 10.
* A 1 when turned up scores 2 for the dealer.
* 5 cards of the same rank
  score 20 points (constituting 10 pairs in all).
  The book calls this combination "royal 5s".
* It's not mentioned in the book,
  but it would make sense to score 1
  for holding an ace of the same suit as the turn up.

Some notes:

* The highestt scoring hand is worth 40 points,
  consisting of 4 fives in hand
  and the fifth one turned up.
* There is only 1 rank worth 10,
  unlike cribbage where there are 4.
  Fives lose a lot of their significance because of this.

#### Hekaton

2 or 4 people may play,
4 in 2 teams of 2.

This is probably the most famous,
and certainly the most original,
game played with these cards.
The goal is to arrange cards
so that they, or their sums,
form the digits of a number
divisible by 100.

When 4 people play
each player is dealt 12 cards.
The two remaining cards can be dealt to elder and dealer
as described in [notes on trick-taking games],
or could be used to start the talon
in which case dealer begins
instead of elder hand.
When two people play
each gets 6 cards,
and draw from the remainder of the deck
after playing a card.

On his turn,
a player places a card,
and has a chance to rearrange
some or all of the cards
to make a number divisible by 100.
A number is made
by making a row of piles
of one or more cards.
The sum of the cards in a pile
(0 counting 0)
represents a "digit" of the number.
For example, if the first pile contains a 1 and a 9,
and the second pile has just a single 0,
then the number is 100,
$`(9+1) \cdot 10 + 0 = 100`.
It's obvious which is the least significant pile,
since a number's first digit can't be 0,
and a multiple of 100 must end with two 0s.

Here's a more mathematically rigorous explanation
of the capturing rules.
If there are $`n > 1` piles
whose sums are equal to $`a_0, a_1, \dots, a_{n-1}`
where $`a_0 + 10 \cdot a_1 \equiv 0 \pmod{100}`
Then the number is equal to:

$$`\sum_{i=0}^{n-1} a_i \cdot {10}^i`

Some more examples of valid numbers:

* With 9, 8 and 2 on the table,
  a player can put 9 in one pile
  and 8 and 2 in the second,
  getting $`9 \cdot 10 + (8 + 2) = 100`.
* 3, 3, 6, 1 on the table,
  and the player plays a 9.
  He can arrange the cards as follows:
  3 in the first pile, 3 and 6 in the second,
  and 9 and 1 in the last.
  The number is $`300 + (3 + 6)\cdot 10 + (9 + 1) = 300 + 90 + 10 = 400`.
* If there was a 9 and a 0,
  and the player played another 0,
  he could make 9, 0, 0 by simple arrangement.

Once a player formed a number,
he wins the number of points equal to it
and puts the cards used in the number away.
Unused cards stay on the talon.
If he can't or doesn't want to make a number,
his played card remains.
Game is 10000 points.
If neither side reaches 10000 points
at the end of a round,
there's another one,
this time elder hand being the dealer.

The possible points that can be scored
grows exponentially as the number of cards increases.
Thus it's possible to score 10000 points in a single move,
winning the game outright.

0s are very powerful cards,
because they bring you a digit closer
to a number divisible by 100.
Thus keep them for as long as possible.

A common trick to make a number
is to make a sum of 10 in the least significant pile,
and a sum of 9 (or 19, etc)
in the pile before it.
The ten gets carried over to the previous column,
causing a 0 to take its place and a hundred to carry over, etc.
Same applies for 20 and 8,
and even 30 and 7,
although it becomes harder and harder
to do it with relatively few cards.

## Critical examination

After giving a technical description of the deck
and the games played with it,
now it's time to examine them as a whole
and look at common game mechanics that define them,
and the themes present in their description and naming.

Before proceeding, I'd like to
make a few things clear about this endeavor:

* All of this is based on one source,
  the 1875 edition of Thomas Frere's hoyle's games.
  It's unclear how faithful its descriptions are
  to the way card games were played at the time,
  even more so to the yankee notion cards.
  Unfortunately, there aren't other sources to my knowledge
  (if anyone knows please [contact me](/contact.html))
  and thus we have to make the most use of it as possible
  but keeping in mind the narrow-range of experience
  contained within it.
* The descriptions of the games
  were likely included to help market the deck,
  and not because the games' players needed a comprehensive reference.
  They were likely written by the designer himself,
  to appeal to what he thought was an average American of his time.

It could be said
that this deck was a vanity project
made by a relatively successsful
but not very memorable publisher,
who tried to capitalize on puritan sensibilities
and feelings of patriotism common in 19th century America
to sell his otherwise unoriginal creation.
First, there are many original elements in this deck,
in particular in its rank and suit structure
and the way it's used in its games.
Secondly, and this is admittedly more sentimental,
I don't believe anything deserves to be completely forgotten,
and I can't imagine a worse fate to befall an idea.

This section is still unfinished.

### Naming

The deck is variously referred to as "Yankee notion" or "Yankee's notion" cards,
the former appearing in the book,
while the other is likely a mistake of other authors.

The name implies a link with T. W. Strong's publication
"Yankee notions" (note the plural "notions")
but I haven't yet investigated much in this direction.
By all accounts the publication was much more popular and influential
than the deck of cards.

The names of pip suits are obviously derived from[american heraldry](https://en.wikipedia.org/wiki/Flag_of_the_United_States)
and are appropriately paired:
flags and the stars on them,
the shields and the eagles (coat of arms) on them.

It lacks face ranks,
but unlike leaving them out like [rook][]
it has instead moved them into their own suit.

[rook]: https://en.wikipedia.org/wiki/Rook_(card_game)

What makes the characters on the face cards unique
is the fact that, unlike in standard playing cards,
they have no obvious or direct links
to the nobility, military or other positions of power.
In fact, most are ordinary and unremarkable,
and aren't prominent in either history or fiction,
with the exception of John Smith,
the 0 of faces.

It's uncertain who John Smith is based on,
if anybody at all.
There's of course the possibility
that he's meant to reference [one of the first settlers of New England](https://en.wikipedia.org/wiki/John_Smith_(explorer))
but it can just as well be a stereotypical American name.
The former is supported by the importance
his card plays in various games in the book,
while the latter is suggested by the lack
of further throwbacks to the historical John Smith,
or indeed any specific John Smith at all.
The book doesn't help matters,
since this is the most we get of his description:

> 0. Is the original John Smith. The only correct likeness ever taken.

The characters don't seem to form a whole,
there are pairs like John and Sally Smith,
Ruth and Ezekiel,
but it's not clear whose the baby is,
or what the watchman watches over, etc.

The face suit also has specific names in some games,
as tilters in [tilters], teasers in [mum],
or victims in [Robin hood](#robin-hood-40-thieves).
While the last name is meant to fit in the theme of the game,
the purpose of the first two isn't clear.
"Tilter" could be referring to fighting or striking
(as in tilting at windmills)
but "teaser" has no such implication,
and in fact implies a more passive role.
In both games faces are trumps,
a term that book uses normally.

There are other terms the book uses,
some of which have unclear origins.
While "graces" is meant to be analogous to "aces",
which is the role 0s often play in these games,
others aren't so obvious.
Zilia (or zills) and peccadilos / peccadils
don't appear anywhere else in the book
and searching didn't bring any relevant information.

